using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script : MonoBehaviour
{
    // Start is called before the first frame update
    
    private Rigidbody rb;
    private Animator animator;
    public float speed = 5f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator= GetComponent<Animator>();
    }

    private void Update()
    {

        float mh = Input.GetAxis("Horizontal");
        float mv = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(mh, 0f, mv).normalized;
        transform.Translate(movement * speed * Time.deltaTime);
        rb.velocity = (movement * speed * Time.deltaTime);
        if(movement != Vector3.zero)
        {
            animator.SetBool("IsMoving", true);
        }
        else
        {
            animator.SetBool("IsMoving", false);
        }

    }

}
